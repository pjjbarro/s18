let trainer = {
	name: "Senku",
	age: 15,
	address: {

		city: "Pallet Town",
		country: "Japan"
	},
	
	friends: ["Taiju","Chrome","Gen"],
	pokemons: ["Giratina","Lugia","Rayquaza","Groudon"]
	catch: function(pokemon){

		if(this.pokemons.length ===6){
			console.log("A trianer should only have 6 pokemons to carry");

		}else {
			this.pokemons.push(pokemon);
			console.log('Gotcha!, ${pokemon}')
		}
	},
	release: function(){
		if(this.pokemon.length === 0){
			console.log("You have no more pokemons. Catch one first!");
		}else{
			this.pokemon.pop();
		}
	};

	function Pokemon(name,type,level){
		this.name = name;
		this.type = type;
		this.level = level;
		this.hp = level *3;
		this.atk = level = 2.5;
		this.def = level = 2.0;
		this.isFainted = false;

		this.tackle = function(target){

			console.log('${this.name} tackled ${target.name}')
			if(target.isFainted !==true && target.hp > 0){
				target.hp -= this.atk;
			}else{
				target.faint();
			}
		}
		this.faint = function(){
			alert('$this.name} has fainted.')
			this.isFainted = true;
		}
	}

	