// Mini-activity

let pcGame = {

	  title: "Dota 2",
	  publisher: "Valve Corporation",
	  year: "2013",
	  price: 250,
	  isAvailable: true
}

console.log(pcGame);
// accessing array items: array[index]
// In an object, keys and values together make up the objects properties.
// accessing object properties: object.property
console.log(pcGame.title);


// Mini-activity

console.log(pcGame.year);
console.log(pcGame.publisher);

// re-assign array items: array[index] = <value>

// re-assigning object properties
pcGame.title = "Final Fantasy X";
console.log(pcGame.title);

// Mini-Activity

pcGame.year = 2001;
pcGame.publisher = "Square Enix";
pcGame.price = 30;

console.log(pcGame.year);
console.log(pcGame.publisher);
console.log(pcGame.price);

// objects can not only have primitive values but it can aslo have arrays and even other objects.

let course = {

	title: "Philosophy 101",
	description: "Learn the values of life",
	price: 5000,
	isActive: true,
	instructors: ["Mr. Johnson", "Mrs. Smith", "Mr. Francis"]

};

console.log(course);
// access the array inside course
console.log(course.instructors);
// access the second item inside the instructors array
console.log(course.instructors[1]);
// delete Mr.Francis from the array
course.instructors.pop();
console.log(course);


// Mini-activity

course.instructors.push("Mr. McGee");
console.log(course.instructors);

let isPresent = course.instructors.includes("Mr.Johnson");
console.log(isPresent);

let instructor = {};
// if you re-assign value to a property that does not exist
// You are able to add a property with a value in our object
instructor.name = "James Johnson";
console.log(instructor);

// Mini-activity

instructor.age = 56,
instructor.gender = "Male";
instructor.department = "Humanities";
instructor.courses = ["Philosophy  101","Humanities 201"];
instructor.isActive = true;
instructor.salary = 50000;

console.log(instructor);

instructor.address = {
	
		street: "#1 Maginhawa St.",
		city: "Quezon City",
		country: "Philippines"
	};

	console.log(instructor);
	// How will we access the street property of our instructor's address property?
	console.log(instructor.address.street);

	// Create objects using a constructor function with similar data structure or keys.
	// This is useful for creating multiple instances of objects

	// "this" keyword when used in a constructor function will allow us to assign a new objects properties and associating with the values recieved from a constructor functions parameters.

	// Constructors are like blueprints or templates to create our objects from
	function Superhero(name,superpower,powerLevel){
		this.name = name;
		this.superpower = superpower;
		this.powerLevel = powerLevel;
	}

	let superhero1 = new Superhero("Saitama","One Punch",30000);
	console.log(superhero1);

	// Mini- Activity

	let superhero2 = new Superhero("Superman","Super Strength","40000");
	let superhero3 = new Superhero("Iron man","Gadgets",10000);
	let superhero4 = new Superhero("Spiderman","Webs",15000);
	console.log(superhero2);
	console.log(superhero3);
	console.log(superhero4);


// Mini-activity

function Laptop(name,brand,price){
	this.name = name;
	this.brand = brand;
	this.price = price;
}

let laptop1 = new Laptop("Ryzen 5","AMD",56000);
// without the new keyword we cannot instantiate a new laptop object from our constructor, instead we can only invoke it.
let laptop2 = new Laptop("Ryzen 9","AMD",84000);

console.log(laptop1);
console.log(laptop2);

// We can create objects with {} (object literals)
// We can create objects with a constructor function and with the use of the keyword

// Object methods
	// A method is a function which is a property of an object
	// In essence it is still a function, however, it is now associated with an object
	// And  to invoke or call the function we have to access the property which contains our function

	let person = {
		name: "Senku",
		talk: function(){
			// When the this keyword is used by a function within an object, it refers to the object where the function is.
			console.log(this);
			console.log('Hi, my name is, what? My name is who? ${this.name}');
		}
	}

	person.talk();

	let person2 = {
		name: "Jones",
		age: 20,
		address: {
			city: "Austin",
			state: "Texas",
			country: "USA"
		},
		friends: ["Milton","Joji"],
		introduce: function(){

			console.log('Hi my name is ${this.name}. I am ${this.age} years old. I live in ${this.address.city},${this.address.state}. My friends are ${this.friends}')
		},
		addFriend: function(str){

			console.log(str);
			// add the argument input into the friends array.
			this.friends.push(str)
			console.log('My friends are now ${this.friends}')
		},
		greet: function(obj){
			console.log(obj.name);
		}
	}

	person2.introduce();
	person2.addFriend("June");
	person2.greet(person);

	// Arrays are special objects with methods that allow us to manipulate them.
	let newArr = [];
	console.log(typeof newArr);

	// Mini-activity